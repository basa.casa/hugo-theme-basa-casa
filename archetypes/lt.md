---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
date: {{ .Date }}
draft: false
courseType: [] # ["Academic","Hobby","Technical","Interpersonal","Interest"]
courseProject: "" # https://gitlab.com/basa.casa/4c-id
dimensions: [""] # ["task-length"]

---

{{ lt Template }}
getJSON ( gitlab API {{.courseProject}})
## Milestones

## Last 10 commits

{{ lt template }}

Development getJSON from issues labeled "development"
Link to add issues

## Support form

## Completion Tasks
(New issue forms. Each issue description template will reference a different branch and milestone as the start state for the problem. Issues will contain instructions links to milestones . Issues should Students will be able to create new instances of the issues, referencing their own fork of the projectIssues will direct learners to complete the rest of the whole task.
